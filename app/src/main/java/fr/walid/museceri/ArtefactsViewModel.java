package fr.walid.museceri;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import java.util.ArrayList;

import fr.walid.museceri.data.MuseumRepository;
import fr.walid.museceri.models.Artefact;

public class ArtefactsViewModel extends AndroidViewModel {

    private MuseumRepository repository;
    private MutableLiveData<Boolean> isLoading;

    private MutableLiveData<ArrayList<Artefact>> allArtefacts;

    public ArtefactsViewModel(@NonNull Application application) {
        super(application);
        ArrayList<Artefact> artefacts=new ArrayList<Artefact>();
        allArtefacts=new MutableLiveData<>();
        repository = MuseumRepository.get(application);
        isLoading=repository.isLoading;
        allArtefacts=repository.getArtefacts();
    }

    LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    LiveData<ArrayList<Artefact>> getAllArtefacts() {
        return allArtefacts;
    }


    public void loadCollection(){
        Thread thread = new Thread(){
            public void run(){
                repository.loadCollection();
            }
        };
        thread.start();
    }

    public void loadCollectionFromDatabase(){
        Thread thread = new Thread(){
            public void run(){
                repository.loadCollectionFromDatabase();
            }
        };
        thread.start();
    }

    public void removeAll(){
        Thread thread = new Thread(){
            public void run(){
                repository.clear();
            }
        };
        thread.start();
    }
    public void deleteArtefact(String id) {
        repository.deleteArtefact(id);
    }

}
