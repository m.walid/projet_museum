package fr.walid.museceri;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.walid.museceri.data.MuseumRepository;
import fr.walid.museceri.models.Artefact;

public class DetailsViewModel extends AndroidViewModel {
    public static final String TAG = DetailsViewModel.class.getSimpleName();
    private MuseumRepository repository;
    private MutableLiveData<Artefact> artefact;
    private  MutableLiveData<Throwable> webServiceThrowable;

    public DetailsViewModel(@NonNull Application application) {
        super(application);
        repository = MuseumRepository.get(application);
        webServiceThrowable=repository.webServiceThrowable;
        artefact=new MutableLiveData<>();
    }
    public void setArtefact(String id){
        repository.getArtefact(id);
        artefact=repository.getSelectedArtefact();
    }
    LiveData<Artefact> getArtefact() {
        return artefact;
    }
    LiveData<Throwable> getWebServiceThrowable(){
        return webServiceThrowable;
    }
}
