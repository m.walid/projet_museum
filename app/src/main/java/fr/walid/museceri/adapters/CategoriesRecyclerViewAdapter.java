package fr.walid.museceri.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;

import fr.walid.museceri.R;
import fr.walid.museceri.data.MuseumRepository;
import fr.walid.museceri.dummy.DummyContent.DummyItem;
import fr.walid.museceri.models.Categorie;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem}.
 * TODO: Replace the implementation with code for your data type.
 */
public class CategoriesRecyclerViewAdapter extends RecyclerView.Adapter<fr.walid.museceri.adapters.CategoriesRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = CategoriesRecyclerViewAdapter.class.getSimpleName();
    //private final List<DummyItem> mValues;
    private ArrayList<String> categories= MuseumRepository.categories;
    public CategoriesRecyclerViewAdapter(List<DummyItem> items) {
        //mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        //holder.mItem = mValues.get(position);
        holder.mIdView.setText(categories.get(position));
        holder.mContentView.setText(categories.get(position));
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public DummyItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.name);
            mContentView = (TextView) view.findViewById(R.id.categories);
            int position=getAdapterPosition();
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=getAdapterPosition();


                }
            });
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}