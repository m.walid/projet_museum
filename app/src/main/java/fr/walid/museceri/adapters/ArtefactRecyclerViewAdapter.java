package fr.walid.museceri.adapters;

import android.provider.ContactsContract;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.loader.app.LoaderManager;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import fr.walid.museceri.ArtefactsFragment;
import fr.walid.museceri.ArtefactsFragmentDirections;
import fr.walid.museceri.ArtefactsViewModel;
import fr.walid.museceri.R;
import fr.walid.museceri.dummy.DummyContent.DummyItem;
import fr.walid.museceri.models.Artefact;
import fr.walid.museceri.util.ImageSolution;


/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem}.
 * TODO: Replace the implementation with code for your data type.
 */
public class ArtefactRecyclerViewAdapter extends RecyclerView.Adapter<fr.walid.museceri.adapters.ArtefactRecyclerViewAdapter.ViewHolder> {
    private static final String TAG = ArtefactRecyclerViewAdapter.class.getSimpleName();
    private ArtefactsViewModel artefactsViewModel;

    private ArrayList<Artefact> artefacts;

    //private final List<DummyItem> mValues;

    public ArtefactRecyclerViewAdapter() {
       //mValues = items;
    }


    private static int i=0;


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=null;
        if(i%2==0) {
            view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_layout11, parent, false);
        }else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_layout2, parent, false);
        }
        i++;
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (artefacts.get(position).getBrand() != null){
            holder.mNameView.setText(artefacts.get(position).getName()+"\n("+artefacts.get(position).getBrand()+")");
        }else{
            holder.mNameView.setText(artefacts.get(position).getName());
        }
        String categorie="";
        int i=0;
        for (String str : artefacts.get(position).getCategories()) {
            if(i==0){
                categorie=categorie+str;
            }else {
                categorie=categorie+" - "+str;
            }
            i++;
        }
        holder.mCategoriesView.setText(categorie);
        ImageSolution.getImage(artefacts.get(position).getId()+"/thumbnail",holder.mImageView);

    }

    @Override
    public int getItemCount() {
        return artefacts == null ? 0 : artefacts.size();
        //return mValues.size();
    }
    public void setArtefactsViewModel(ArtefactsViewModel v) {
        artefactsViewModel = v;
    }
    
    public void setArtefacts(ArrayList<Artefact> artefacts) {
        this.artefacts=artefacts;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNameView;
        public final TextView mCategoriesView;
        public final ImageView mImageView;
        //public DummyItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameView = (TextView) view.findViewById(R.id.name);
            mCategoriesView = (TextView) view.findViewById(R.id.categories);
            mImageView=(ImageView) view.findViewById(R.id.imageView2);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG,"position="+getAdapterPosition());
                    String id= ArtefactRecyclerViewAdapter.this.artefacts.get((int)getAdapterPosition()).getId();
                    Log.d(TAG,"id="+id);
                    ArtefactsFragmentDirections.ToDetailsFragment action=ArtefactsFragmentDirections.toDetailsFragment(id);
                    action.setItemId(id);
                    NavOptions navOptions = new NavOptions.Builder()
                            .setPopUpTo(R.id.artefactsFragment, true)
                            .build();
                    try {
                        Navigation.findNavController(v).navigate(action);
                    }catch (Exception e){
                        System.out.println("error rabek "+v.toString());
                    }

                }
            });
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mCategoriesView.getText() + "'";
        }


    }


}