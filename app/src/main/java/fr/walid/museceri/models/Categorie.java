package fr.walid.museceri.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "categories_database", indices = {@Index(value = {"_id", "name"}, unique = true)})
public class Categorie {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name="_id")
    private long id;


    @NonNull
    @ColumnInfo(name="name")
    private String name;

    public Categorie(long id, @NonNull String name) {
        this.id = id;
        this.name = name;
    }

    public Categorie() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }
}
