package fr.walid.museceri.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Arrays;
import java.util.List;

import fr.walid.museceri.util.DataConvertInt;
import fr.walid.museceri.util.DataConverter;

@Entity(tableName = "artefact_database", indices = {@Index(value = {"name", "description"}, unique = true)})
public class Artefact {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name="_id")
    private String id;

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @NonNull
    @TypeConverters(DataConverter.class)
    @ColumnInfo(name="categories")
    private String[] categories;

    @NonNull
    @TypeConverters(DataConvertInt.class)
    @ColumnInfo(name="timeFrame")
    private int[] timeFrame;

    @NonNull
    @ColumnInfo(name="description")
    private String description;


    @ColumnInfo(name="year")
    private int year;


    @ColumnInfo(name="brand")
    private String brand;


    @TypeConverters(DataConverter.class)
    @ColumnInfo(name="technicalDetails")
    private String[] technicalDetails;


    @ColumnInfo(name="working")
    private boolean working;

    public Artefact(String name, String[] categories, int[] timeFrame, String description, int year, String brand, String[] technicalDetails, boolean working) {
        this.name = name;
        this.categories = categories;
        this.timeFrame = timeFrame;
        this.description = description;
        this.year = year;
        this.brand = brand;
        this.technicalDetails = technicalDetails;
        this.working = working;
    }

    public Artefact() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getCategories() {
        return categories;
    }

    public void setCategories(String[] categories) {
        this.categories = categories;
    }

    public int[] getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(int[] timeFrame) {
        this.timeFrame = timeFrame;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String[] getTechnicalDetails() {
        return technicalDetails;
    }

    public void setTechnicalDetails(String[] technicalDetails) {
        this.technicalDetails = technicalDetails;
    }

    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    @Override
    public String  toString() {
        return "Artefact{" +
                "name='" + name + '\'' +
                ", categories=" + Arrays.toString(categories) +
                ", timeFrame=" + Arrays.toString(timeFrame) +
                ", description='" + description + '\'' +
                ", year=" + year +
                ", brand='" + brand + '\'' +
                ", technicalDetails=" + Arrays.toString(technicalDetails) +
                ", working=" + working +
                '}';
    }
}
