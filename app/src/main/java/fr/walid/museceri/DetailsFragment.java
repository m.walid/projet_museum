package fr.walid.museceri;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.google.android.material.snackbar.Snackbar;

public class DetailsFragment extends Fragment {
    public static final String TAG = DetailsFragment.class.getSimpleName();
    private DetailsViewModel viewModel;
    private TextView tName, tDescription, tCategories, tTimeFrame, tYear, tBrand, tTechnicalDetails,tWorking;
    private ImageView imageView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        return inflater.inflate(R.layout.details_fragment, container, false);
    }


    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(DetailsViewModel.class);

        DetailsFragmentArgs args = DetailsFragmentArgs.fromBundle(getArguments());
        String argsItemId = args.getItemId();
        viewModel.setArtefact(argsItemId);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {
        tName = getView().findViewById(R.id.nameD);
        tCategories=getView().findViewById(R.id.categorie);
    }
    private void observerSetup() {
        viewModel.getArtefact().observe(getViewLifecycleOwner(),
                artefact -> {
                    if (artefact != null) {
                        tName.setText(artefact.getName());
                        String categorie="";
                        int i=0;
                        for (String str : artefact.getCategories()) {
                            if(i==0){
                                categorie=categorie+str;
                            }else {
                                categorie=categorie+" - "+str;
                            }
                            i++;
                        }
                        tCategories.setText(categorie);
                    }
                });
        viewModel.getWebServiceThrowable().observe(getViewLifecycleOwner(),
                throwable ->{
                    Snackbar snackbar = Snackbar
                            .make(getView(), throwable.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
        );
    }


}
