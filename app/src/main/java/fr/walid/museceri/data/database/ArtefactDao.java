package fr.walid.museceri.data.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import fr.walid.museceri.models.Artefact;


@Dao
public interface ArtefactDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Artefact artefact);

    @Query("SELECT * from artefact_database ORDER BY name ASC")
    List<Artefact> getAllArtefacts();

    @Query("SELECT * FROM artefact_database WHERE _id = :id")
    Artefact getArtefactById(String id);

    @Query("DELETE FROM artefact_database WHERE _id = :id")
    void deleteArtefact(String id);

    @Query("DELETE FROM artefact_database")
    void deleteAll();


}
