package fr.walid.museceri.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.walid.museceri.data.database.ArtefactDao;
import fr.walid.museceri.data.database.ArtefactRoomDatabase;
import fr.walid.museceri.data.database.CategorieDao;
import fr.walid.museceri.data.database.CategorieRoomDatabase;
import fr.walid.museceri.models.Artefact;
import fr.walid.museceri.models.Categorie;
import fr.walid.museceri.requests.ArtefactsApi;
import fr.walid.museceri.requests.Responses.ArtefactResponse;
import fr.walid.museceri.requests.Responses.CategorieResponse;
import fr.walid.museceri.requests.Results.ArtefactResult;
import fr.walid.museceri.requests.Results.CategorieResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.walid.museceri.data.database.ArtefactRoomDatabase.databaseWriteExecutor;

public class MuseumRepository {
    private static final String TAG = MuseumRepository.class.getSimpleName();

    private LiveData<List<Artefact>> allArtefacts;
    private MutableLiveData<Artefact> selectedArtefact=new MutableLiveData<>();

    public MutableLiveData<Boolean> isLoading=new MutableLiveData<Boolean>();
    public MutableLiveData<Throwable> webServiceThrowable=new MutableLiveData<>();

    private final ArtefactsApi api1;
    public static ArrayList<String> categories= new ArrayList<>();

    private ArtefactDao artefactDao;
    private CategorieDao categorieDao;
    volatile int nbAPILoads=0;
    private MutableLiveData<ArrayList<Artefact>> artefacts=new MutableLiveData<>();
    //private MutableLiveData<ArrayList<Categorie>> categories=new MutableLiveData<>();

    private static volatile MuseumRepository INSTANCE;

    public synchronized static MuseumRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new MuseumRepository(application);
        }
        return INSTANCE;
    }
    public MutableLiveData<ArrayList<Artefact>> getArtefacts() {
        return artefacts;
    }

    public MuseumRepository(Application application) {
        ArtefactRoomDatabase db = ArtefactRoomDatabase.getDatabase(application);
        CategorieRoomDatabase db2= CategorieRoomDatabase.getDatabase(application);
        artefactDao = db.artefactDao();
        categorieDao= db2.categorieDao();
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();
        api1 = retrofit.create(ArtefactsApi.class);
    }

    public MutableLiveData<Artefact> getSelectedArtefact() {
        return selectedArtefact;
    }

    public void clear(){
        artefactDao.deleteAll();
        artefacts.postValue(null);
    }

    public void loadCollectionFromDatabase(){
        ArrayList<Artefact> allArtefacts= (ArrayList<Artefact>) artefactDao.getAllArtefacts();
        //ArrayList<Categorie> allCategories=(ArrayList<Categorie>) allArtefacts.getAllCategories();
        artefacts.postValue(allArtefacts);
        //categories.postValue(allCategories);
    }

    public void loadCollection(){
        isLoading.postValue(Boolean.TRUE);
        api1.getCollection().enqueue(
                new Callback<Map<String, ArtefactResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String, ArtefactResponse>> call,
                                           Response<Map<String, ArtefactResponse>> response) {
                        Log.d("API",response.body().toString());
                        ArrayList<Artefact> artefactArrayList=new ArrayList<>();
                        int i=0;
                        for (String key: response.body().keySet()) {
                            ArtefactResult.transferInfoArtefact(response.body().get(key), key, artefactArrayList);
                            long res= insertArtefact(artefactArrayList.get(i));
                            i++;
                        }
                        artefacts.setValue(artefactArrayList);
                        //isLoading.postValue(Boolean.FALSE);
                    }
                    @Override
                    public void onFailure(Call<Map<String, ArtefactResponse>> call, Throwable t) {
                        Log.d("failled to load",t.getMessage());
                        if (nbAPILoads<=1){
                            isLoading.postValue(Boolean.FALSE);
                        }
                        else{
                            nbAPILoads=nbAPILoads-1;
                        }
                        webServiceThrowable.postValue(t);
                    }
                });
        api1.getCategories().enqueue(
                new Callback<List<String>>() {

                    @Override
                    public void onResponse(Call<List<String>> call, Response<List<String>> response) {

                        for (int i=0;i<18;i++) {
                            insertCategorie(categories.get(i));
                        }
                    }

                    @Override
                    public void onFailure(Call<List<String>> call, Throwable t) {

                    }
                }
        );

    }
    public long insertArtefact(Artefact artefact) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return artefactDao.insert(artefact);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;
    }
    public void insertCategorie(String categorie) {
        this.categories.add(categorie);
    }


    public void getArtefact(String id) {

        Future<Artefact> fartefact = databaseWriteExecutor.submit(() -> {
            return artefactDao.getArtefactById(id);
        });
        try {
            selectedArtefact.setValue(fartefact.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void deleteArtefact(String id) {
        databaseWriteExecutor.execute(() -> {
            artefactDao.deleteArtefact(id);
        });
    }

    public void getThumbnail(String itemId){

    }


}
