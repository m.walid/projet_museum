package fr.walid.museceri.data.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.walid.museceri.models.Categorie;
@Database(entities = {Categorie.class}, version = 4, exportSchema = false)
public abstract class CategorieRoomDatabase  extends RoomDatabase {
    private static final String TAG = CategorieRoomDatabase.class.getSimpleName();

    public abstract CategorieDao categorieDao();

    private static CategorieRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor= Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static CategorieRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (CategorieRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            CategorieRoomDatabase.class,"categories_database")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){
                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                    databaseWriteExecutor.execute(() -> {
                        CategorieDao dao = INSTANCE.categorieDao();
                    });
                }
            };
}
