package fr.walid.museceri.requests;

import java.util.List;
import java.util.Map;

import fr.walid.museceri.models.Artefact;
import fr.walid.museceri.models.Categorie;
import fr.walid.museceri.requests.Responses.ArtefactResponse;
import fr.walid.museceri.requests.Responses.CategorieResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface ArtefactsApi {
    @Headers("Accept: application/json")
    @GET("collection")
    Call<Map< String , ArtefactResponse>> getCollection();
    @GET("items/{itemID}/thumbnail")
    void getImage(@Path("itemID") String imageName, Callback<ArtefactResponse> callback);

    @Headers("Accept: application/json")
    @GET("collection")
    Call<List<String>> getCategories();

}
