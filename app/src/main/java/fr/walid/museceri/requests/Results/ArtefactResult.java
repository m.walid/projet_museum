package fr.walid.museceri.requests.Results;

import java.util.ArrayList;

import fr.walid.museceri.models.Artefact;
import fr.walid.museceri.requests.Responses.ArtefactResponse;

public class ArtefactResult {
    public static void transferInfoArtefact(ArtefactResponse body, String key, ArrayList<Artefact> artefacts) {

        Artefact artefact = new Artefact();
        artefact.setId(key);
        artefact.setName(body.name);
        artefact.setDescription(body.description);
        artefact.setCategories(body.categories);
        artefact.setTimeFrame(body.timeFrame);


        artefact.setWorking(body.working);
        if (body.year != -1){
            artefact.setYear(body.year);
        }
        if (body.brand!=null){
            artefact.setBrand(body.brand);
        }
        if(body.technicalDetails!=null){
            artefact.setTechnicalDetails(body.technicalDetails);
        }
        artefact.setWorking(body.working);




        artefacts.add(artefact);

    }
}
