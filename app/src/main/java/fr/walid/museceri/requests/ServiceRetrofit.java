package fr.walid.museceri.requests;

import fr.walid.museceri.util.Constants;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class ServiceRetrofit {

    private static Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create());
    private static Retrofit retrofit = retrofitBuilder.build();

    private static ArtefactsApi artefactsApi = retrofit.create(ArtefactsApi.class);

    public ArtefactsApi getArtefactsApi(){
        return artefactsApi;
    }
}
