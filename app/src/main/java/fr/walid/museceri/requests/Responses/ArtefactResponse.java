package fr.walid.museceri.requests.Responses;

import fr.walid.museceri.models.Artefact;

public class ArtefactResponse {
    public String name=null;
    public String[] categories=null;
    public int[] timeFrame=null;
    public String description=null;
    public int year=-1;
    public String brand=null;
    public String[] technicalDetails=null;
    public boolean working=false;

}
