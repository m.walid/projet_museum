package fr.walid.museceri;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import fr.walid.museceri.ArtefactsFragment;
import fr.walid.museceri.adapters.ArtefactRecyclerViewAdapter;
import fr.walid.museceri.dummy.DummyContent;

/**
 * A fragment representing a list of Items.
 */
public class ArtefactsFragment extends Fragment {
    private View view;
    private boolean isfragmentvisible=false;

    public static fr.walid.museceri.ArtefactsFragment getInstance(){
        fr.walid.museceri.ArtefactsFragment artefactsFragment=new fr.walid.museceri.ArtefactsFragment();
        return artefactsFragment;
    }

    public static final String TAG = ArtefactsFragment.class.getSimpleName();

    private ArtefactsViewModel viewModel;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ArtefactRecyclerViewAdapter adapter;
    private ProgressBar progress;


    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    /*blic ArtefactsFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static fr.walid.museceri.ArtefactsFragment newInstance(int columnCount) {
        fr.walid.museceri.ArtefactsFragment fragment = new fr.walid.museceri.ArtefactsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }
    */


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_artefacts, container, false);

        // Set the adapter

        return view;
    }
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        this.view = view;

        new CountDownTimer(5000, 1000) { //Wait 5 seconds
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() { // Then navigate to other page.

                //check if fragment is visible or not
                if(isfragmentvisible){
                    navigate();
                }

            }
        }.start();

        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(ArtefactsViewModel.class);


        listenerSetup();

        observerSetup();

    }

    @Override
    public void onResume() {
        super.onResume();
        isfragmentvisible=true;
    }

    @Override
    public void onPause() {
        super.onPause();
        isfragmentvisible=false;
    }
    private void navigate() {
        //Navigation.findNavController(this.view).navigate(ArtefactsFragmentDirections.toDetailsFragment());
    }
    private void listenerSetup(){
        recyclerView = getView().findViewById(R.id.list);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ArtefactRecyclerViewAdapter();
        recyclerView.setAdapter(adapter);
        adapter.setArtefactsViewModel(viewModel);
        //progress = getView().findViewById(R.id.progress_bar);


    }
    private void observerSetup() {
        viewModel.getAllArtefacts().observe(getViewLifecycleOwner(),
                artefacts -> adapter.setArtefacts(artefacts));
/*
        viewModel.getIsLoading().observe(getViewLifecycleOwner(),
                isLoading ->{
                    if(isLoading){
                        progress.setVisibility(View.VISIBLE);
                    }
                    else{
                        progress.setVisibility(View.GONE);
                    }
                }
        );
        viewModel.getIsLoading().observe(getViewLifecycleOwner(),
                isLoading ->{
                    if(isLoading){
                        progress.setVisibility(View.VISIBLE);
                    }
                    else{
                        progress.setVisibility(View.GONE);
                    }
                }
        );*/

    }
}
