package fr.walid.museceri.util;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class DataConverter implements Serializable {

    @TypeConverter // note this annotation
    public String fromStringsList(String[] strings) {
        if (strings == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<String[]>() {
        }.getType();
        String json = gson.toJson(strings, type);
        return json;
    }

    @TypeConverter // note this annotation
    public String[] toStringsList(String optionValuesString) {
        if (optionValuesString == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<String[]>() {
        }.getType();
        String[] productCategoriesList = gson.fromJson(optionValuesString, type);
        return productCategoriesList;
    }

}