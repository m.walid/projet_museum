package fr.walid.museceri.util;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;

public class DataConvertInt implements Serializable {

    @TypeConverter // note this annotation
    public String fromIntArray(int[] ints) {
        if (ints == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<int[]>() {
        }.getType();
        String json = gson.toJson(ints, type);
        return json;
    }

    @TypeConverter // note this annotation
    public int[] toIntArray(String string) {
        if (string == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<int[]>() {
        }.getType();
        int[] productCategoriesList = gson.fromJson(string, type);
        return productCategoriesList;
    }
}
